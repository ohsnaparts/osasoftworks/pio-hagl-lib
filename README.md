# PlatformIO HAGL Lib

This library adds a thin wrapper around the [Hardware Agnostic Graphics library][HAGL] by adding a [PlatformIO Library] declaration to be able to conveniently reference it in [PlatformIO] projects.




Please see [library.json] for up-to-date references and information on the authors.

## Description

**Roughly**, to put it in OOP terms, [HAGL] relies on dependency
inversion to provide an abstraction layer between the application and the actual hardware (canvas, display). By relying on a `HAL interface` for drawing, the user can provide its own canvas. The application only has to know about `HAGL`, but no details further down. In terms of `C` this is of course merely linker magic and no actual objects or classes are involved.

```plantuml

class application {
    + draw_screen()
}

interface "HAGL HAL" as hal {
    + initialize()
    + draw_pixel()
    + flush()
}

class "ST7735 HAL" as hal_st7735 {
    + initialize_spi()
    --
    - write_spi_pixel()
    - write_spi_flush()
}

class "GD HAL" as hal_gdlib {
    + initialize_gd()
    --
    - draw_gd_pixel()
    - flush_gd_image()
}

class "HAGL" as hagl {
    + fill_circle()
    + draw_line()
    + draw_char()
}

application --> hagl: " draws using"
hal_gdlib --o hal: " implements"
hal_st7735 --o hal: " implements"
hagl ..> hal_gdlib: " indirectly\ndraws\nusing"
hagl ..> hal_st7735: "indirectly\ndraws\nusing"
hagl -> hal: " draws using"
```

## Requirements

This library relies on git submodules to reference the [HAGL] library.

```git
git submodule init
git submodule update
```

Furthermore, it depends on a [HAGL HAL] implementation.
Please see [library.json] for namings.

## Usage

1. create a platform io project
1. add `pio-hagl` and `pio-hagl-hal` repos as submodules in `/lib`
1. import and use `hagl.h`

## Example

To prevent recursive submodule references, and
keep the exmaple real, it has been implemented
in a separate repository

[PlatformIO Library]: https://docs.platformio.org/en/latest/manifests/library-json/index.html
[PlatformIO]: https://platformio.org/
[HAGL]: https://github.com/tuupola/hagl.git
[HAGL HAL]: https://github.com/tuupola/hagl#backend
[library.json]: ./library.json